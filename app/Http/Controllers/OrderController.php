<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\DB;
use App\Services\OrderService;
use Exception;

class OrderController extends Controller
{

    public function store(OrderRequest $request)
    {
        DB::beginTransaction();
        try {
            $order = new OrderService($request->currency, $request->count);
            $result = $order->process();

            return response()->json(['success' => $result]);
            DB::commit();
        } catch (Exception $e) {
            return response()->json(['failed' => $e->getMessage()]);

            DB::rollBack();
        }
    }
}
