<?php

namespace App\Services;

use App\Models\Currency;
use Exception;

class OrderService
{

    private $currency;
    private $count;
    private $price;
    private $userBalance = 2000;
    private $currencyModel;

    public function __construct(string $currency, int $count)
    {
        $this->currency = $currency;
        $this->count = $count;
        $this->currencyModel = resolve(Currency::class);
        $this->setPrice();
    }

    private function setPrice()
    {
        $this->price = $this->currencyModel->getCurrencies()[$this->currency]['price'] * $this->count;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function process(): bool
    {
        $this->deductMoney();
        if ($this->price < 10) {
            $this->accumulateOrder();
            return true;
        }

        return $this->buyFromExchange();
    }

    private function deductMoney()
    {
        if ($this->userBalance <= $this->price) {
            throw new Exception('Insufficient balance error');
        }

        $this->userBalance -= $this->price;
        //Then update balance in database
    }

    private function accumulateOrder()
    {
        // Accumulate the order for batch processing later
        // You can implement the accumulation logic here
    }

    private function buyFromExchange(): bool
    {
        // Call the HTTP request to international exchanges to process the order
        // You can implement the actual API call logic here
        // Return true if the order is successfully processed, or false otherwise
        return true;
    }
}
