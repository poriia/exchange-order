<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\OrderService;

class OrderTest extends TestCase
{
    public function testOrderProcessing()
    {
        $order = new OrderService('ABAN', 1);
        $this->assertEquals('ABAN', $order->getCurrency());
        $this->assertEquals(1, $order->getCount());

        $this->assertTrue($order->process());
    }
}
