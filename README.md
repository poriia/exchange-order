## Project Setup Guide

This is a simple API for placing purchase orders for a cryptocurrency exchange. It is built using the Lumen framework, Docker, Nginx, and PHP 8.


#### Prerequisites

Make sure you have the following dependencies installed on your machine:

- Docker
- Docker Compose

#### Getting Started

Follow the steps below to set up and run the project:

1 - Clone the repository

```
git clone git@gitlab.com:poriia/exchange-order.git
```
2 - Navigate to the project directory:
```
cd <project-directory>
```
3 - Build and start the Docker containers:
```
docker compose up -d
```
4 - Install the project dependencies:
```
docker compose exec app composer install
```
5 - The API is now up and running! You can access it at http://localhost:8000.

#### API Documentation

Place an Order
- Endpoint: /orders
- Method: POST
- Request Body:
    - `currency` (string, required): The name of the cryptocurrency to purchase.
    - `amount` (numeric, required): The amount of the cryptocurrency to purchase.
- Example Request:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"currency":"ABAN","amount":1}' http://localhost:8000/orders
```

- Example Response (Success):
```json
{
  "success": true
}
```
- Example Response (Validation Error):
```json
{
  "error": {
    "currency": ["The currency field is required."],
    "amount": ["The amount field is required."]
  }
}
```

#### Running Tests
To run the test suite, execute the following command:

```
docker compose exec app vendor/bin/phpunit
```

#### Project Structure

The main files and directories of the project are structured as follows:

- `app/`: Contains the application code, including the controllers, models, and other business logic.
- `database/`: Contains the database migrations.
- `routes/`: Contains the API route definitions.
- `tests/`: Contains the PHPUnit test cases.

#### Troubleshooting
- If you encounter any issues related to ports being already in use, make sure to stop any conflicting services running on the same ports (e.g., Apache, Nginx).
- If you make changes to the Docker configuration or the Dockerfile, you may need to rebuild the Docker containers using the docker-compose build command.

That's it! You have successfully set up the project and can now start placing purchase orders through the API.