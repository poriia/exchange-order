FROM php:8-fpm

WORKDIR /var/www/aban-tether

RUN apt-get update && apt-get install -y \
    libzip-dev \
    unzip

RUN docker-php-ext-install \
    zip \
    pdo_mysql

COPY . /var/www/aban-tether

RUN chown -R www-data:www-data /var/www/aban-tether/storage /var/www/aban-tether/bootstrap/cache


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer update --no-interaction --no-dev --optimize-autoloader

CMD php artisan serve --host=0.0.0.0 --port=8000
